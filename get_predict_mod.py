import code
import pandas as pd
import glob
import os
import librosa
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import specgram
import soundfile as sf
import sounddevice as sd
import queue
import keras

def extract_feature(file_name=None):
    if file_name: 
        print('Extracting', file_name)
        X, sample_rate = sf.read(file_name, dtype='float32')
    else:  
        device_info = sd.query_devices(None, 'input')
        sample_rate = int(device_info['default_samplerate'])
        q = queue.Queue()
        def callback(i,f,t,s): q.put(i.copy())
        data = []
        with sd.InputStream(samplerate=sample_rate, callback=callback):
            while True: 
                if len(data) < 100000: data.extend(q.get())
                else: break
        X = np.array(data)

    if X.ndim > 1: X = X[:,0]
    X = X.T

    # short term fourier transform
    stft = np.abs(librosa.stft(X))

    # mfcc (mel-frequency cepstrum)
    mfccs = np.mean(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=40).T,axis=0)

    # chroma
    chroma = np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T,axis=0)

    # melspectrogram
    mel = np.mean(librosa.feature.melspectrogram(X, sr=sample_rate).T,axis=0)

    # spectral contrast
    contrast = np.mean(librosa.feature.spectral_contrast(S=stft, sr=sample_rate).T,axis=0)

    tonnetz = np.mean(librosa.feature.tonnetz(y=librosa.effects.harmonic(X), sr=sample_rate).T,axis=0)
    return mfccs,chroma,mel,contrast,tonnetz

def parse_predict_files(parent_dir,file_ext='*.ogg'):
    features = np.empty((0,193))
    filenames = []
    # print(glob.glob(os.path.join(parent_dir, file_ext)))
    for fn in glob.glob(os.path.join(parent_dir, file_ext)):
        mfccs, chroma, mel, contrast,tonnetz = extract_feature(fn)
        ext_features = np.hstack([mfccs,chroma,mel,contrast,tonnetz])
        features = np.vstack([features,ext_features])
        filenames.append(fn)
        print("extract %s features done" % fn)
    return np.array(features), np.array(filenames)

def get_predict_mod(features_cough, features_ww):
  
  # Predict them
  model_ww = keras.models.load_model('models/trained_model_ww.h5', compile=False)
  X_predict = np.expand_dims(features_ww, axis=2)
  pred = model_ww.predict(np.array(X_predict))
  prob_ww = pred
 
  # Predict cough
  model_cough = keras.models.load_model('models/trained_model_cough.h5')
  X_predict = np.expand_dims(features_cough, axis=2)
  pred = model_cough.predict(np.array(X_predict))
  prob_cough = pred
 
  # Se guarda el df
  list_probs = [prob_cough[0], prob_ww[0]]
  df = pd.DataFrame(list_probs, columns=["Probs"])
  df.to_csv('Probs.csv', index=False)
  
  return([prob_cough, prob_ww])

def main():
    # Predict tos, them
    features_cough, filenames = parse_predict_files('audio', "tos.wav")
    features_ww, filenames = parse_predict_files('audio', "them.wav")
    return(get_predict_mod(features_cough, features_ww))
